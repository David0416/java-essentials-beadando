import java.util.Scanner;

public class Calculator {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    double height;
    while (true) {
      System.out.println("Írja be a magasságát:");
      try {
        height = Double.parseDouble(sc.next());
        break;
      } catch (NumberFormatException ignore) {
        System.out.println("Invalid input");
      }
    }

    double weight;
    while (true) {
      System.out.println("Írja be a súlyát:");
      try {
        weight = Double.parseDouble(sc.next());
        break;
      } catch (NumberFormatException ignore) {
        System.out.println("Invalid input");
      }
    }
    sc.close();
    double heightCM = heightToCM(height);
    double weightKG = weightToKG(weight);

    double BMIvalue = BMI(heightCM, weightKG);

    if (Double.isInfinite(BMIvalue)) {
      System.out.println("Értelmezhetetlen érték");
    } else {
      String kiir = meaningOfValue(BMIvalue);

      System.out.println("A BMI indexed: " + BMIvalue + ", ami így jellemzi a testedet: " + kiir);
    }
  }

  // 140-210cm közötti értékekre
  private static double heightToCM(double height) {
    double heightCM = 0;
    if (height > 140 && height < 210) {
      heightCM = height;
    } else if (height > 1.4 && height < 2.1) {
      heightCM = height * 100;
    } else if (height > 1400 && height < 2100) {
      heightCM = height / 10;
    } else if (height > 55.118 && height < 82.677) {
      heightCM = height * 2.54;
    } else if (height > 4.593 && height < 6.889) {
      heightCM = height * 30.483;
    }
    return heightCM;
  }

  // 40-140kg közötti értékekre
  private static double weightToKG(double weight) {
    double weightKG = 0;
    if (weight > 40 && weight < 140) {
      weightKG = weight;
    } else if (weight > 88.1849 && weight < 308.647) {
      weightKG = weight / 2.2046;
    }
    return weightKG;
  }

  // BMI kiszámítása
  private static double BMI(double height, double weight) {
    double BMIvalue = 0;

    BMIvalue = weight / (height / 100 * height / 100);

    return BMIvalue;
  }

  // amit jelenet az adott érték
  private static String meaningOfValue(double BMIvalue) {
    String kiirando;

    if (BMIvalue < 16) {
      kiirando = "Severe Thinness";
    } else if (BMIvalue < 17) {
      kiirando = "Moderate Thinness";
    } else if (BMIvalue < 18.5) {
      kiirando = "Mild Thinness";
    } else if (BMIvalue < 25) {
      kiirando = "Normal";
    } else if (BMIvalue < 30) {
      kiirando = "Overweight";
    } else if (BMIvalue < 35) {
      kiirando = "Obese Class I";
    } else if (BMIvalue < 40) {
      kiirando = "Obese Class II";
    } else {
      kiirando = "Obese Class III";
    }
    return kiirando;
  }
}